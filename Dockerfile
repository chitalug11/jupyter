FROM kitware/pulse:4.1.0

################################################################################
# install some python packages
COPY requirements.txt /tmp/
RUN pip3 install --requirement /tmp/requirements.txt
RUN jupyter nbextension enable --py bqplot

################################################################################
# set up user
ARG NB_USER=notebooks
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

WORKDIR ${HOME}



# Copy our notebooks to ${HOME}
COPY *.ipynb ${HOME}/
COPY *.py ${HOME}/
# Copy pulse data files to ${HOME}/data
RUN mkdir ${HOME}/data
RUN mv /pulse/bin/config/ ${HOME}/data/
RUN mv /pulse/bin/ecg/ ${HOME}/data/
RUN mv /pulse/bin/environments/ ${HOME}/data/
RUN mv /pulse/bin/nutrition/ ${HOME}/data/
RUN mv /pulse/bin/patients/ ${HOME}/data/
RUN mv /pulse/bin/states/ ${HOME}/data/
RUN mv /pulse/bin/substances/ ${HOME}/data/
RUN mv /pulse/bin/*.json ${HOME}/data
RUN mv /pulse/bin/PyPulse* ${HOME}
# Copy pulse python files to ${HOME}
RUN mv /pulse/python/ ${HOME}/python/
ENV PYTHONPATH ${HOME}:${HOME}/python
# Change ownership to the binder runner
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}
